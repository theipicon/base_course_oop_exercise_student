package Modules;

import Entities.MissionItems.MissileType;

public class AttackModule extends MissionModule {
    private int numOfMissiles;
    private MissileType missileType;

    public AttackModule() {
        this.numOfMissiles = 0;
        missileType = null;
    }

    public AttackModule(int numOfMissiles, MissileType missileType) {
        this.numOfMissiles = numOfMissiles;
        this.missileType = missileType;
    }

    public int numOfMissiles() {
        return this.numOfMissiles;
    }

    public MissileType missileType() {
        return this.missileType;
    }

    public void changeNumOfMissiles(int numOfMissiles) {
        this.numOfMissiles = numOfMissiles;
    }

    public void changeMissileType(MissileType missileType) {
        this.missileType = missileType;
    }
}
