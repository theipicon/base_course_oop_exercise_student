package Modules;

import Entities.MissionItems.CameraType;

public class BdaModule extends MissionModule {
    private CameraType cameraType;

    public BdaModule() {
        this.cameraType = null;
    }

    public BdaModule(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    public void changeCameraType(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    public CameraType cameraType() {
        return this.cameraType;
    }
}
