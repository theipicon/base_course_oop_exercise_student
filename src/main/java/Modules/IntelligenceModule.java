package Modules;

import Entities.MissionItems.SensorType;

public class IntelligenceModule extends MissionModule {
    private SensorType sensorType;

    public IntelligenceModule() {
        this.sensorType = null;
    }

    public IntelligenceModule(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public SensorType sensorType() {
        return this.sensorType;
    }

    public void changeSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }
}
