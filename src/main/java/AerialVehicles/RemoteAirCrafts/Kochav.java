package AerialVehicles.RemoteAirCrafts;

import AerialVehicles.AirCraftSuperTypes.Hermes;
import Entities.Coordinates;
import Modules.AttackModule;
import Modules.BdaModule;
import Modules.IntelligenceModule;

public class Kochav extends Hermes {
    public Kochav(Coordinates homeCoordinates) {
        super(homeCoordinates);

        this.addNewModule(new IntelligenceModule());
        this.addNewModule(new AttackModule());
        this.addNewModule(new BdaModule());
    }
}
