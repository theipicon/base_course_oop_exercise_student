package AerialVehicles.RemoteAirCrafts;


import AerialVehicles.AirCraftSuperTypes.Hermes;
import Entities.Coordinates;
import Modules.BdaModule;
import Modules.IntelligenceModule;

public class Zik extends Hermes {
    public Zik(Coordinates homeCoordinates) {
        super(homeCoordinates);

        this.addNewModule(new IntelligenceModule());
        this.addNewModule(new BdaModule());
    }
}