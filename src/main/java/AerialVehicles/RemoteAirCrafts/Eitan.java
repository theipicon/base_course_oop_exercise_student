package AerialVehicles.RemoteAirCrafts;

import AerialVehicles.AirCraftSuperTypes.Haron;
import Entities.Coordinates;
import Modules.AttackModule;
import Modules.IntelligenceModule;

public class Eitan extends Haron {
    public Eitan(Coordinates homeCoordinates) {
        super(homeCoordinates);

        this.addNewModule(new IntelligenceModule());
        this.addNewModule(new AttackModule());
    }
}
