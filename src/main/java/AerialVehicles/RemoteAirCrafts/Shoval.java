package AerialVehicles.RemoteAirCrafts;


import AerialVehicles.AirCraftSuperTypes.Haron;
import Entities.Coordinates;
import Modules.AttackModule;
import Modules.BdaModule;
import Modules.IntelligenceModule;

public class Shoval extends Haron {
    public Shoval(Coordinates homeCoordinates) {
        super(homeCoordinates);

        this.addNewModule(new IntelligenceModule());
        this.addNewModule(new BdaModule());
        this.addNewModule(new AttackModule());
    }
}

