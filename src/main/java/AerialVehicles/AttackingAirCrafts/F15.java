package AerialVehicles.AttackingAirCrafts;


import AerialVehicles.AirCraftSuperTypes.AttackingAirCraft;
import Entities.Coordinates;
import Modules.AttackModule;
import Modules.IntelligenceModule;

public class F15 extends AttackingAirCraft {
    public F15(Coordinates homeCoordinates) {
        super(homeCoordinates);

        this.addNewModule(new AttackModule());
        this.addNewModule(new IntelligenceModule());
    }
}
