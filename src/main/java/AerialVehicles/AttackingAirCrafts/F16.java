package AerialVehicles.AttackingAirCrafts;


import AerialVehicles.AirCraftSuperTypes.AttackingAirCraft;
import Entities.Coordinates;
import Modules.AttackModule;
import Modules.BdaModule;

public class F16 extends AttackingAirCraft {
    public F16(Coordinates homeCoordinates) {
        super(homeCoordinates);

        this.addNewModule(new AttackModule());
        this.addNewModule(new BdaModule());
    }
}
