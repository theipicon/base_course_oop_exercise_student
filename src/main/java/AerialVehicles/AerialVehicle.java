package AerialVehicles;

import Entities.Coordinates;
import Entities.FlightStatus;
import Modules.MissionModule;

import java.util.ArrayList;
import java.util.List;

public abstract class AerialVehicle {
    private final int maxHoursWithoutRepair;

    private int hoursFromLastRepair;
    private FlightStatus flightStatus;
    private Coordinates homeCoordinates;
    private List<MissionModule> availableModules;

    public AerialVehicle(int maxHoursWithoutRepair, Coordinates homeCoordinates) {
        this.maxHoursWithoutRepair = maxHoursWithoutRepair;
        this.hoursFromLastRepair = 0;
        this.flightStatus = FlightStatus.READY_FOR_FLIGHT;
        this.homeCoordinates = homeCoordinates;
        this.availableModules = new ArrayList<>();
    }

    private int hoursFromLastRepair() {
        return this.hoursFromLastRepair;
    }

    public FlightStatus flightStatus() {
        return this.flightStatus;
    }

    private int maxHoursWithoutRepair() {
        return this.maxHoursWithoutRepair;
    }

    private void changeHoursFromLastRepair(int hoursFromLastRepair) {
        this.hoursFromLastRepair = hoursFromLastRepair;
    }

    public Coordinates homeCoordinates() {
        return this.homeCoordinates;
    }

    protected void changeFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public List<MissionModule> availableModules() {
        return this.availableModules;
    }

    protected void addNewModule(MissionModule moduleToAdd) {
        this.availableModules.add(moduleToAdd);
    }

    public void flyTo(Coordinates destination) {
        if (this.flightStatus() == FlightStatus.READY_FOR_FLIGHT) {
            this.changeFlightStatus(FlightStatus.IN_AIR);

            System.out.println("Flying to: " + destination.longitude() + ", " + destination.latitude() + ".");
        } else {
            System.out.println("Aerial Vehicle isn't ready to fly.");
        }
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on: " + destination.longitude() + ", " + destination.latitude() + ".");

        this.check();
    }

    private void check() {
        if(this.hoursFromLastRepair() >= this.maxHoursWithoutRepair()) {
            this.changeFlightStatus(FlightStatus.NOT_READY_FOR_FLIGHT);

            this.repair();
        } else {
            this.changeFlightStatus(FlightStatus.READY_FOR_FLIGHT);
        }
    }

    private void repair() {
        this.changeHoursFromLastRepair(0);
        this.changeFlightStatus(FlightStatus.READY_FOR_FLIGHT);
    }
}
