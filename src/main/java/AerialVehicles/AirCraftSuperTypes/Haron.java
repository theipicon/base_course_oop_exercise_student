package AerialVehicles.AirCraftSuperTypes;

import Entities.Coordinates;
import Entities.MaxHoursWithoutRepair;

public abstract class Haron extends RemoteAirCraft {
    public Haron(Coordinates homeCoordinates) {
        super(MaxHoursWithoutRepair.HARON_AIR_CRAFTS.getHours(), homeCoordinates);
    }
}
