package AerialVehicles.AirCraftSuperTypes;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Entities.MaxHoursWithoutRepair;

public abstract class AttackingAirCraft extends AerialVehicle {
    public AttackingAirCraft(Coordinates homeCoordinates) {
        super(MaxHoursWithoutRepair.ATTACKING_AIR_CRAFTS.getHours(),homeCoordinates);
    }
}
