package AerialVehicles.AirCraftSuperTypes;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Entities.FlightStatus;

public abstract class RemoteAirCraft extends AerialVehicle {

    public RemoteAirCraft(int maxHoursWithoutRepair, Coordinates homeCoordinates) {
        super(maxHoursWithoutRepair, homeCoordinates);
    }

    public String hoverOverLocation(Coordinates destination) {
        String hoverOver = "Hovering Over: " + destination.longitude() + ", " + destination.longitude() + ".";

        this.changeFlightStatus(FlightStatus.IN_AIR);
        System.out.println(hoverOver);

        return hoverOver;
    }
}
