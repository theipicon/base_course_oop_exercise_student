package AerialVehicles.AirCraftSuperTypes;

import Entities.Coordinates;
import Entities.MaxHoursWithoutRepair;

public abstract class Hermes extends RemoteAirCraft {
    public Hermes(Coordinates homeCoordinates) {
        super(MaxHoursWithoutRepair.HEMRES_AIR_CRAFTS.getHours(), homeCoordinates);
    }
}
