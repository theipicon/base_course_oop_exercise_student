package Entities.MissionItems;

public enum CameraType {
    REGULAR,
    THERMAL,
    NIGHT_VISION
}
