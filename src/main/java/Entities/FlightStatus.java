package Entities;

public enum FlightStatus {
    READY_FOR_FLIGHT,
    NOT_READY_FOR_FLIGHT,
    IN_AIR
}
