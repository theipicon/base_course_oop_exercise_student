package Entities;

public enum MaxHoursWithoutRepair {
    ATTACKING_AIR_CRAFTS(250),
    HARON_AIR_CRAFTS(150),
    HEMRES_AIR_CRAFTS(100);


    private final int maxHoursWithoutRepair;

    MaxHoursWithoutRepair(int maxHoursWithoutRepair) {
        this.maxHoursWithoutRepair = maxHoursWithoutRepair;
    }

    public int getHours() {
        return this.maxHoursWithoutRepair;
    }
}
