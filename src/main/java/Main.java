import AerialVehicles.AttackingAirCrafts.F15;
import AerialVehicles.AttackingAirCrafts.F16;
import AerialVehicles.RemoteAirCrafts.Eitan;
import AerialVehicles.RemoteAirCrafts.Zik;
import Entities.Coordinates;
import Entities.MissionItems.CameraType;
import Entities.MissionItems.MissileType;
import Entities.MissionItems.SensorType;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import Modules.AttackModule;
import Modules.BdaModule;
import Modules.IntelligenceModule;

public class Main {
    public static void main(String[] args) {
        //Test 1: Checking execution of missions
        F16 f16 = new F16(new Coordinates(3.0, 2.0));
        F15 f15 = new F15(new Coordinates(5.0, 20.0));

        AttackModule f16AttackModule = (AttackModule) f16.availableModules().get(0);
        BdaModule f16bdaModule = (BdaModule) f16.availableModules().get(1);
        IntelligenceModule f15IntelligenceModule = (IntelligenceModule) f15.availableModules().get(1);

        System.out.println("-------------------------TEST-1-------------------------");

        try {
            AttackMission attackMission = new AttackMission("Muhamad", new Coordinates(4.0, 20.0), "Maxik", f16);

            f16AttackModule.changeMissileType(MissileType.AMRAM);
            f16AttackModule.changeNumOfMissiles(25);

            attackMission.begin();
            attackMission.finish();

            BdaMission bdaMission = new BdaMission("kaka", new Coordinates(4.0, 3.0), "meow", f16);

            f16bdaModule.changeCameraType(CameraType.NIGHT_VISION);

            bdaMission.begin();
            bdaMission.finish();

            IntelligenceMission intelligenceMission = new IntelligenceMission("very sad region", new Coordinates(20.0, 15.0), "helloName", f15);

            f15IntelligenceModule.changeSensorType(SensorType.ELINT);

            intelligenceMission.begin();
            intelligenceMission.cancel();

            intelligenceMission.begin();
            intelligenceMission.finish();

            // Should throw an exception
            IntelligenceMission intelligenceMissionFailed = new IntelligenceMission("very sad region", new Coordinates(20.0, 15.0), "helloName", f16);
        } catch (AerialVehicleNotCompatibleException ex) {
            System.out.println(ex.getMessage());
        }

        // Test 2: Checking different aircraft methods
        System.out.println("-------------------------TEST-2-------------------------");

        Eitan eitan = new Eitan(new Coordinates(3.0, 4.0));

        eitan.flyTo(new Coordinates(20.0, 35.5));
        eitan.land(new Coordinates(20.0, 69.99));
        eitan.hoverOverLocation(new Coordinates(42.69, 1.1));

        // Test 3: One big check from one to finish
        System.out.println("-------------------------TEST-3-------------------------");

        Zik zik = new Zik(new Coordinates(78.1, 8.4));
        IntelligenceModule zikIntelligenceModule = (IntelligenceModule) zik.availableModules().get(0);

        zik.flyTo(new Coordinates(20.0, 35.5));
        zik.land(new Coordinates(20.0, 69.99));
        zik.hoverOverLocation(new Coordinates(42.69, 1.1));

        try {
            IntelligenceMission intelligenceMission = new IntelligenceMission("regionTest", new Coordinates(1.1, 2.2), "I AM YOOUR FATHER LUKE", zik);

            zikIntelligenceModule.changeSensorType(SensorType.INFRA_RED);

            intelligenceMission.begin();
            intelligenceMission.cancel();

            intelligenceMission.begin();
            intelligenceMission.finish();

            // Should throw an exception
            AttackMission attackMission = new AttackMission("targetTest", new Coordinates(5.5,5.5), "AVADA KADABRA", zik);
        } catch (AerialVehicleNotCompatibleException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
