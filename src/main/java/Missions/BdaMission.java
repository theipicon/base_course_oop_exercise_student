package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Modules.BdaModule;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(String objective, Coordinates missionCoordinates, String pilotsName, AerialVehicle aerialVehicle) throws AerialVehicleNotCompatibleException {
        super(missionCoordinates, pilotsName, aerialVehicle, BdaModule.class);
        this.objective = objective;
    }

    private String objective() {
        return this.objective;
    }

    @Override
    public String executeMission() {
        BdaModule currentModule = (BdaModule) this.findModule();

        assert currentModule != null;

        return this.pilotsName() + ": " + this.aerialVehicle().getClass().getSimpleName() + " taking pictures of suspect " + this.objective() + " with: " + currentModule.cameraType();
    }
}
