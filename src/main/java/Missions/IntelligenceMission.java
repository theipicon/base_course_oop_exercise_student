package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Modules.IntelligenceModule;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(String region, Coordinates missionCoordinates, String pilotsName, AerialVehicle aerialVehicle) throws AerialVehicleNotCompatibleException{
        super(missionCoordinates, pilotsName, aerialVehicle, IntelligenceModule.class);
        this.region = region;
    }

    private String region() {
        return this.region;
    }

    @Override
    public String executeMission() {
        IntelligenceModule currentModule = (IntelligenceModule) this.findModule();

        assert currentModule != null;

        return this.pilotsName() + ": " + this.aerialVehicle().getClass().getSimpleName() + " Collecting Data in " + this.region() + " with: " + currentModule.sensorType();
    }
}
