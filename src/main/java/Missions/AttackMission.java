package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Modules.AttackModule;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(String target, Coordinates missionCoordinates, String pilotsName, AerialVehicle aerialVehicle) throws AerialVehicleNotCompatibleException {
        super(missionCoordinates, pilotsName, aerialVehicle, AttackModule.class);
        this.target = target;
    }

    private String target() {
        return this.target;
    }

    @Override
    public String executeMission() {
        AttackModule currentModule = (AttackModule) this.findModule();

        assert currentModule != null;

        return this.pilotsName() + ": " + this.aerialVehicle().getClass().getSimpleName() + " Attacking suspect " + this.target() + " with: " + currentModule.missileType() + "X" + currentModule.numOfMissiles();
    }
}
