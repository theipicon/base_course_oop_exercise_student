package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Entities.FlightStatus;
import Modules.MissionModule;

public abstract class Mission {
    private Coordinates missionCoordinates;
    private String pilotsName;
    private AerialVehicle aerialVehicle;
    private Class<? extends MissionModule> relevantModule;

    public Mission(Coordinates missionCoordinates, String pilotsName, AerialVehicle aerialVehicle, Class<? extends MissionModule> relevantModule) throws AerialVehicleNotCompatibleException {
        this.missionCoordinates = missionCoordinates;
        this.pilotsName = pilotsName;
        this.aerialVehicle = aerialVehicle;
        this.relevantModule = relevantModule;

        this.checkValidity();
    }

    protected MissionModule findModule() {
        return this.aerialVehicle().availableModules()
                .stream()
                .filter(module -> module.getClass().getSimpleName().equals(this.relevantModule().getSimpleName()))
                .findFirst()
                .orElse(null);
    }

    private void checkValidity() throws AerialVehicleNotCompatibleException {
        if (this.findModule() == null) {
            throw new AerialVehicleNotCompatibleException(this.aerialVehicle().getClass().getSimpleName() + " doesn't support " + this.relevantModule().getSimpleName());
        }
    }

    public Class<? extends MissionModule> relevantModule() {
        return this.relevantModule;
    }

    protected Coordinates missionCoordinates() {
        return this.missionCoordinates;
    }

    protected AerialVehicle aerialVehicle() {
        return this.aerialVehicle;
    }

    protected String pilotsName() {
        return this.pilotsName;
    }

    public void begin() {
        if (this.aerialVehicle().flightStatus() == FlightStatus.READY_FOR_FLIGHT) {
            this.aerialVehicle().flyTo(this.missionCoordinates());

            System.out.println("Beginning Mission!");
        } else {
            this.cancel();
        }
    }

    public void cancel() {
        this.aerialVehicle().land(this.aerialVehicle().homeCoordinates());

        System.out.println("Abort Mission!");
    }

    public abstract String executeMission();

    public void finish() {
        System.out.println(this.executeMission());
        this.aerialVehicle().land(this.aerialVehicle().homeCoordinates());

        System.out.println("Finish Mission!");
    }
}
